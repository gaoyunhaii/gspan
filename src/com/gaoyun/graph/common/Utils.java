package com.gaoyun.graph.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Utils {
	public static <T> void increCount(Map<T, Integer> counters, T t, int incre) {
		Integer original = counters.get(t);
		if (original == null) {
			counters.put(t, incre);
		} else {
			counters.put(t, original.intValue() + incre);
		}
	}

	public static <K, V> void addToMapList(Map<K, ArrayList<V>> map, K k, V v) {
		ArrayList<V> values = map.get(k);
		if (values == null) {
			values = new ArrayList<V>();
			map.put(k, values);
		}

		values.add(v);
	}
}
