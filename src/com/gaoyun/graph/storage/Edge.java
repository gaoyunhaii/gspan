package com.gaoyun.graph.storage;

public class Edge {
	private int from;
	private int to;
	private int label;

	private boolean isRemoved;

	public int getFrom() {
		return from;
	}

	public void setFrom(int from) {
		this.from = from;
	}

	public int getTo() {
		return to;
	}

	public void setTo(int to) {
		this.to = to;
	}

	public int getLabel() {
		return label;
	}

	public void setLabel(int label) {
		this.label = label;
	}

	public boolean isRemoved() {
		return isRemoved;
	}

	public void setRemoved(boolean isRemoved) {
		this.isRemoved = isRemoved;
	}

	@Override
	public String toString() {
		return "Edge [from=" + from + ", to=" + to + ", label=" + label + ", isRemoved=" + isRemoved + "]";
	}

}
