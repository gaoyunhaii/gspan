package com.gaoyun.graph.storage;

import java.util.ArrayList;
import java.util.List;

public class Vertex {
	private int id;
	private int label;

	private List<Edge> edges = new ArrayList<Edge>();

	private boolean isRemoved;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLabel() {
		return label;
	}

	public void setLabel(int label) {
		this.label = label;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}

	public boolean isRemoved() {
		return isRemoved;
	}

	public void setRemoved(boolean isRemoved) {
		this.isRemoved = isRemoved;
	}

	@Override
	public String toString() {
		return "Vertex [id=" + id + ", label=" + label + "isRemoved=" + isRemoved + "]";
	}
	
}
