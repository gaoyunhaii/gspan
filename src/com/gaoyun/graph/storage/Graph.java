package com.gaoyun.graph.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {
	private int id;
	private List<Edge> edges = new ArrayList<Edge>();
	private List<Vertex> vertices = new ArrayList<Vertex>();
	private Map<Integer, Vertex> indexToVertex = new HashMap<Integer, Vertex>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Vertex> getVertices() {
		return vertices;
	}

	public void setVertices(List<Vertex> vertices) {
		this.vertices = vertices;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}

	public Map<Integer, Vertex> getIndexToVertex() {
		return indexToVertex;
	}

	public void setIndexToVertex(Map<Integer, Vertex> indexToVertex) {
		this.indexToVertex = indexToVertex;
	}

	void addVertex(int id, int label) {
		Vertex vertex = new Vertex();
		vertex.setId(id);
		vertex.setLabel(label);

		this.vertices.add(vertex);
		this.indexToVertex.put(id, vertex);
	}

	public void addEdge(int from, int to, int label) {
		if(from == to){
			System.out.println("add edge fount the same edge: graph = " + getId() + ", from = " + from + ", to = " + to);
		}
		
		Edge edge = new Edge();
		edge.setFrom(from);
		edge.setTo(to);
		edge.setLabel(label);

		this.edges.add(edge);

		Vertex vertex = this.indexToVertex.get(from);
		if (vertex == null) {
			throw new RuntimeException("vertex " + from + " not found");
		}
		vertex.getEdges().add(edge);

		vertex = this.indexToVertex.get(to);
		if (vertex == null) {
			throw new RuntimeException("vertex " + from + " not found");
		}
		vertex.getEdges().add(edge);
	}

	public void removeVertexByLabel(int label) {
		for (Vertex v : this.vertices) {
			if (v.getLabel() == label) {
				v.setRemoved(true);
				List<Edge> edges = v.getEdges();
				for (Edge e : edges) {
					e.setRemoved(true);
				}
			}
		}
	}

	public void removeEdgeByLabel(int label) {
		for (Edge e : this.edges) {
			if (e.getLabel() == label) {
				e.setRemoved(true);
			}
		}
	}

	public void relabelVertex(Map<Integer, Integer> oldToNew) {
		for (Vertex v : this.vertices) {
			if(!v.isRemoved()){
				v.setLabel(oldToNew.get(v.getLabel()));
			}
		}
	}

	public void relabelEdge(Map<Integer, Integer> oldToNew) {
		for (Edge e : this.edges) {
			if(!e.isRemoved()){
				e.setLabel(oldToNew.get(e.getLabel()));
			}
		}
	}
	
	public int getVertexLabel(int id){
		return this.indexToVertex.get(id).getLabel();
	}
}
