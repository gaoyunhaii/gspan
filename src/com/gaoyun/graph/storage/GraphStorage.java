package com.gaoyun.graph.storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;

import com.gaoyun.graph.common.Utils;

public class GraphStorage {
	private List<Graph> graphs = new ArrayList<Graph>();
	private Map<Integer, Graph> indexToGraph = new HashMap<Integer, Graph>();

	private Map<Integer, Integer> vertexLabelCount = new HashMap<Integer, Integer>();
	private Map<Integer, Integer> edgeLabelCount = new HashMap<Integer, Integer>();

	public GraphStorage() {

	}

	public List<Graph> getGraphs() {
		return graphs;
	}

	public void setGraphs(List<Graph> graphs) {
		this.graphs = graphs;
	}

	public Map<Integer, Graph> getIndexToGraph() {
		return indexToGraph;
	}

	public void setIndexToGraph(Map<Integer, Graph> indexToGraph) {
		this.indexToGraph = indexToGraph;
	}

	public Map<Integer, Integer> getVertexLabelCount() {
		return vertexLabelCount;
	}

	public void setVertexLabelCount(Map<Integer, Integer> vertexLabelCount) {
		this.vertexLabelCount = vertexLabelCount;
	}

	public Map<Integer, Integer> getEdgeLabelCount() {
		return edgeLabelCount;
	}

	public void setEdgeLabelCount(Map<Integer, Integer> edgeLabelCount) {
		this.edgeLabelCount = edgeLabelCount;
	}

	public void readFromFile(String path) throws Exception {
		FileInputStream fin = new FileInputStream(new File(path));
		Scanner scanner = new Scanner(fin);

		Graph currentGraph = null;
		Set<Integer> currentGraphVertexLabels = new HashSet<Integer>();
		Set<Integer> currentGraphEdgeLabels = new HashSet<Integer>();

		while (true) {
			try {
				String token = scanner.next();
				if (token.equals("t")) {
					scanner.next(); // skip #
					int gid = scanner.nextInt();
					if (gid == -1) { // in fact means end
						break;
					}

					currentGraph = new Graph();

					for (Integer label : currentGraphVertexLabels) {
						Utils.increCount(vertexLabelCount, label, 1);
					}

					for (Integer label : currentGraphEdgeLabels) {
						Utils.increCount(edgeLabelCount, label, 1);
					}

					currentGraphVertexLabels.clear();
					currentGraphEdgeLabels.clear();

					currentGraph.setId(gid);
					this.graphs.add(currentGraph);
					this.indexToGraph.put(gid, currentGraph);
				} else if (token.equals("v")) {
					int tid = scanner.nextInt();
					int label = scanner.nextInt();

					// System.out.println("add vertex " + tid + "," + label);

					currentGraph.addVertex(tid, label);

					// update vertex label
					// Utils.increCount(vertexLabelCount, label, 1);
					currentGraphVertexLabels.add(label);

				} else if (token.equals("e")) {
					int from = scanner.nextInt();
					int to = scanner.nextInt();
					int label = scanner.nextInt();

					currentGraph.addEdge(from, to, label);

					// System.out.println("add edge " + from + "," + to + ","
					// + label);

					// Utils.increCount(edgeLabelCount, label, 1);
					currentGraphEdgeLabels.add(label);
				}
			} catch (NoSuchElementException e) {
				break;
			}
		}

		for (Integer label : currentGraphVertexLabels) {
			Utils.increCount(vertexLabelCount, label, 1);
		}

		for (Integer label : currentGraphEdgeLabels) {
			Utils.increCount(edgeLabelCount, label, 1);
		}

		System.out.println(this.graphs.size());
		for (Graph g : this.graphs) {
			System.out.println(g.getVertices().size() + ","
					+ g.getEdges().size());
		}
	}

	public void removeVertexByLabel(int label) {
		for (Graph g : this.graphs) {
			g.removeVertexByLabel(label);
		}
	}

	public void removeEdgeByLabel(int label) {
		for (Graph g : this.graphs) {
			g.removeEdgeByLabel(label);
		}
	}

	public void relabelVertex(Map<Integer, Integer> oldToNew) {
		for (Graph g : this.graphs) {
			g.relabelVertex(oldToNew);
		}
	}

	public void relabelEdge(Map<Integer, Integer> oldToNew) {
		for (Graph g : this.graphs) {
			g.relabelEdge(oldToNew);
		}
	}

	public static void main(String[] args) {
		GraphStorage gs = new GraphStorage();
		try {
			gs.readFromFile("graph.data");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
