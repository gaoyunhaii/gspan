package com.gaoyun.graph.gspan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GSpanTmpGraph {
	public static class TmpGraphEdge {
		TmpGraphVertex from;
		TmpGraphVertex to;

		int label;
	}

	public static class TmpGraphVertex {
		int label;
		int id;
		int index = -1;

		List<TmpGraphEdge> edges = new ArrayList<TmpGraphEdge>();
	}

	public static class TmpGraphDFSEdge extends DFSEdge {

		TmpGraphEdge edge;

		public TmpGraphDFSEdge(int fromIndex, int toIndex, int fromLabel,
				int eLabel, int toLabel) {
			super(fromIndex, toIndex, fromLabel, eLabel, toLabel);
		}

	}

	List<TmpGraphVertex> vertices = new ArrayList<TmpGraphVertex>();
	Map<Integer, TmpGraphVertex> indexToVertices = new HashMap<Integer, TmpGraphVertex>();
	ArrayList<DFSTreeNode> originalNodes = new ArrayList<DFSTreeNode>();

	public GSpanTmpGraph(DFSTreeNode node) {
		DFSTreeNode tmp = node;
		LinkedList<DFSTreeNode> hehe = new LinkedList<DFSTreeNode>();
		
		while (tmp != null) {
			hehe.addFirst(tmp);
			tmp = tmp.getParent();
		}
		this.originalNodes.addAll(hehe);

		// now generate the graph from the originalNodes
		for (DFSTreeNode tn : this.originalNodes) {
			int fromIndex = tn.getLastEdge().getFromIndex();
			int toIndex = tn.getLastEdge().getToIndex();
			int fromLabel = tn.getLastEdge().getFromLabel();
			int toLabel = tn.getLastEdge().getToLabel();
			int eLabel = tn.getLastEdge().geteLabel();

			TmpGraphVertex fromVertex = this.getVertexOrCreate(fromIndex,
					fromLabel);
			TmpGraphVertex toVertex = this.getVertexOrCreate(toIndex, toLabel);

			TmpGraphEdge edge = new TmpGraphEdge();
			edge.from = fromVertex;
			edge.to = toVertex;
			edge.label = eLabel;

			fromVertex.edges.add(edge);
			toVertex.edges.add(edge);
		}
	}

	private int dfsMinThan(TmpGraphVertex current,
			Map<Integer, TmpGraphVertex> index, int nextToCompare, TmpGraphEdge parentEdge) {
		// for a vertex, there should be only one way to get the minimum code
		List<TmpGraphDFSEdge> backwardEgdes = new ArrayList<TmpGraphDFSEdge>();
		List<TmpGraphDFSEdge> forwardEdges = new ArrayList<TmpGraphDFSEdge>();
		
		//backward edge first, then all the forward edge
		for (TmpGraphEdge edge : current.edges) {
			if(edge == parentEdge){
				continue;
			}
			
			TmpGraphVertex vTo = edge.to;
			if(edge.from.id != current.id){
				vTo = edge.from;
			}
			
			if (vTo.index < 0) {// forward edge
				TmpGraphDFSEdge te = new TmpGraphDFSEdge(current.index, index.size(),
						current.label, edge.label, vTo.label);
				te.edge = edge;
				forwardEdges.add(te);
			} else {
				TmpGraphDFSEdge te = new TmpGraphDFSEdge(current.index, vTo.index,
						current.label, edge.label, vTo.label);
				te.edge = edge;
				backwardEgdes.add(te);
			}
		}
		
		Collections.sort(backwardEgdes);
		Collections.sort(forwardEdges);
		
		for(TmpGraphDFSEdge be : backwardEgdes){
			int com = be.compareTo(this.originalNodes.get(nextToCompare++).getLastEdge());
			if(com != 0){
				return com;
			}
		}
		
		//then we handle the forward edge
		for(TmpGraphDFSEdge fw : forwardEdges){
			TmpGraphVertex vTo = fw.edge.to;
			if(fw.edge.from.id != current.id){
				vTo = fw.edge.from;
			}
			
			if(vTo.index > 0){ //this edge has been accessed;
				continue;
			}
			
			vTo.index = index.size();
			index.put(vTo.index, vTo);
			
			//compare
			int com = fw.compareTo(this.originalNodes.get(nextToCompare++).getLastEdge());
			if(com != 0){
				return com;
			}
			
			int re = dfsMinThan(vTo, index, nextToCompare, fw.edge);
			if(re != 0){
				return re;
			}
		}
		
		return 0;
	}

	public boolean isMin() {
		//System.out.println(originalNodes);
		for (TmpGraphVertex vertex : this.vertices) {// start dfs from this
														// vertex
			Map<Integer, TmpGraphVertex> newIndex = new HashMap<Integer, TmpGraphVertex>();

			vertex.index = 0;
			newIndex.put(0, vertex);
			int minThan = this.dfsMinThan(vertex, newIndex, 0, null);
			if (minThan < 0) {
				return false;
			}

			for (TmpGraphVertex v : this.vertices) {
				v.index = -1;
			}
		}

		return true;
	}

	private TmpGraphVertex getVertexOrCreate(int index, int label) {
		TmpGraphVertex vertex = this.indexToVertices.get(index);
		if (vertex == null) {
			vertex = new TmpGraphVertex();
			vertex.label = label;
			vertex.id = this.vertices.size();
			this.indexToVertices.put(index, vertex);
			this.vertices.add(vertex);
		}

		return vertex;
	}

}
