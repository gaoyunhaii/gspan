package com.gaoyun.graph.gspan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gaoyun.graph.common.Utils;
import com.gaoyun.graph.storage.Edge;
import com.gaoyun.graph.storage.Graph;

public class LevelOneDFSEdge extends DFSEdge {

	private Map<Integer, ArrayList<GSpanGraphContext>> contexts;

	public LevelOneDFSEdge(int fromIndex, int toIndex, int fromLabel, int eLabel, int toLabel) {
		super(fromIndex, toIndex, fromLabel, eLabel, toLabel);
	}

	public void addOccurence(Graph g, Edge e, int from, int to) {
		if (contexts == null) {
			contexts = new HashMap<Integer, ArrayList<GSpanGraphContext>>();
		}

		GSpanGraphContext context = new GSpanGraphContext();
		context.assignIdIndex(from, 0);
		context.assignIdIndex(to, 1);
		context.setFirstEdge(e);
		
		//add to my contextToEdge
		this.contextToEdge.put(context.getId(), e);
		this.contextToEdgeFrom.put(context.getId(), from);

		Utils.addToMapList(contexts, g.getId(), context);
	}

	public Map<Integer, ArrayList<GSpanGraphContext>> getContexts() {
		return contexts;
	}
}
