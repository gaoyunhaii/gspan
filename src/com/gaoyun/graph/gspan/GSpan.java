package com.gaoyun.graph.gspan;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

import com.gaoyun.graph.common.Utils;
import com.gaoyun.graph.gspan.GSpanTmpGraph.TmpGraphDFSEdge;
import com.gaoyun.graph.gspan.GSpanTmpGraph.TmpGraphEdge;
import com.gaoyun.graph.gspan.GSpanTmpGraph.TmpGraphVertex;
import com.gaoyun.graph.storage.Edge;
import com.gaoyun.graph.storage.Graph;
import com.gaoyun.graph.storage.GraphStorage;
import com.gaoyun.graph.storage.Vertex;

public class GSpan {
	private GraphStorage graphStorage;
	private double support;
	private int min;

	// other shared structure
	private List<Integer> vertexLabel = new ArrayList<Integer>();
	private List<Integer> edgeLabel = new ArrayList<Integer>();
	private Map<Integer, Integer> newToOldVertexLabel = new HashMap<Integer, Integer>();
	private Map<Integer, Integer> newToOldEdgeLabel = new HashMap<Integer, Integer>();

	private List<DFSTreeNode> results = new ArrayList<DFSTreeNode>();

	public GSpan(GraphStorage graphStorage, double support) {
		super();
		this.graphStorage = graphStorage;
		this.support = support;
	}

	private boolean isMinCode(DFSTreeNode current) {
		GSpanTmpGraph tmpGraph = new GSpanTmpGraph(current);
		return tmpGraph.isMin();
	}

	private int getOtherEnd(Edge e, int from) {
		if (e.getFrom() == from) {
			return e.getTo();
		}

		return e.getFrom();
	}

	private List<DFSEdge> getBackwardEdgeList(Graph g, GSpanGraphContext context, int fromId, Integer parentId) {
		List<DFSEdge> result = new ArrayList<DFSEdge>();

		Vertex vertex = g.getVertices().get(fromId);
		int fromIndex = context.getIdToIndex().get(fromId);

		for (Edge edge : vertex.getEdges()) {
			if (edge.isRemoved()) {
				continue;
			}

			int toId = getOtherEnd(edge, fromId);
			if (toId == parentId) {
				continue;
			}

			Integer toIndex = context.getIdToIndex().get(toId);
			
			if (toIndex != null) {
				DFSEdge de = new DFSEdge(fromIndex, toIndex, vertex.getLabel(), edge.getLabel(),
						g.getVertexLabel(toId));
				result.add(de);
			}
		}

		Collections.sort(result);
		return result;
	}

	private List<DFSEdge> getForwardEdgeList(Graph g, GSpanGraphContext context, int fromId) {
		List<DFSEdge> result = new ArrayList<DFSEdge>();

		Vertex vertex = g.getVertices().get(fromId);
		int fromIndex = context.getIdToIndex().get(fromId);

		for (Edge edge : vertex.getEdges()) {
			if (edge.isRemoved()) {
				continue;
			}

			int toId = getOtherEnd(edge, fromId);
			Integer toIndex = context.getIdToIndex().get(toId);

			if (toIndex == null) {
				DFSEdge de = new DFSEdge(fromIndex, context.getIdToIndex().size(), vertex.getLabel(), edge.getLabel(),
						g.getVertexLabel(toId));
				result.add(de);
			}
		}

		Collections.sort(result);
		return result;
	}

	private Map<Integer, ArrayList<GSpanGraphContext>> countAndGenereateBackward(
			Map<Integer, ArrayList<GSpanGraphContext>> old, DFSEdge newlyAdded) {
		int fromIndex = newlyAdded.getFromIndex();
		int toIndex = newlyAdded.getToIndex();

		Map<Integer, ArrayList<GSpanGraphContext>> mine = new HashMap<Integer, ArrayList<GSpanGraphContext>>();
		for (Map.Entry<Integer, ArrayList<GSpanGraphContext>> e : old.entrySet()) {
			Graph g = this.graphStorage.getGraphs().get(e.getKey());

			for (GSpanGraphContext context : e.getValue()) {
				int fromId = context.getIndexToId().get(fromIndex);
				int toId = context.getIndexToId().get(toIndex);

				// see if we have a edge here and has the same label
				boolean found = false;
				for (Edge tmp : g.getVertices().get(fromId).getEdges()) {
					if(tmp.isRemoved()){
						continue;
					}
					
					
					if (getOtherEnd(tmp, fromId) == toId && tmp.getLabel() == newlyAdded.geteLabel()) {
						found = true;
						break;
					}
				}

				if (found) {
					Utils.addToMapList(mine, g.getId(), context);
				}
			}
		}

		return mine;
	}

	private Map<Integer, ArrayList<GSpanGraphContext>> countAndGenereateForward(
			Map<Integer, ArrayList<GSpanGraphContext>> old, DFSEdge newlyAdded) {
		//System.out.println("forward: " + newlyAdded);
		Map<Integer, ArrayList<GSpanGraphContext>> mine = new HashMap<Integer, ArrayList<GSpanGraphContext>>();
		
		int fromIndex = newlyAdded.getFromIndex();
		int eLabel = newlyAdded.geteLabel();
		int toLabel = newlyAdded.getToLabel();
		
		for (Map.Entry<Integer, ArrayList<GSpanGraphContext>> e : old.entrySet()) {
			Graph g = this.graphStorage.getGraphs().get(e.getKey());
			for (GSpanGraphContext context : e.getValue()) {
				
				for(Edge tmp : g.getVertices().get(fromIndex).getEdges()){
					if(tmp.isRemoved()){
						continue;
					}
					
					int fromId = context.getIndexToId().get(fromIndex);
					int toId = getOtherEnd(tmp, fromId);
					Integer possibleToIndex = context.getIdToIndex().get(toId);
					if(possibleToIndex != null){
						continue;
					}
					
					if(tmp.getLabel() == eLabel && g.getVertexLabel(toId) == toLabel){
						//found one
						GSpanGraphContext newContext = null;
						try {
							newContext = context.clone();
						} catch (CloneNotSupportedException e1) {
							e1.printStackTrace();
						}
						
						//we also need to record this new physical edge
						//System.out.println("toId = " + toId + ", toIndex = " + newlyAdded.getToIndex());
						newContext.assignIdIndex(toId, newlyAdded.getToIndex());
						newlyAdded.getContextToEdge().put(newContext.getId(), tmp);
						newlyAdded.getContextToEdgeFrom().put(newContext.getId(), fromId);
						
//						System.out.println("old:" + context.getIdToIndex() + "," + context.getIndexToId());
//						System.out.println("new:" + newContext.getIdToIndex() + "," + newContext.getIndexToId());
						
						Utils.addToMapList(mine, g.getId(), newContext);
					}
				}
				
			}
		}
		
		return mine;
	}

	private void subgraphMining(DFSTreeNode current, boolean isLevelOne, int level) {
		System.out.println("mining level" + level);
		if (!isLevelOne) {
			// 1. adjust whether this node is min
			boolean isMin = this.isMinCode(current);
			if (!isMin) {
				return;
			}
		}
		// add this to result
		this.results.add(current);

		DFSEdge lastEdge = current.getLastEdge();

		// we'll rewrite the logic
		// now we extend current by add one edge
		// now we will have to grow this node
		Set<DFSEdge> newlyAddedEdge = new HashSet<DFSEdge>();
		for (Map.Entry<Integer, ArrayList<GSpanGraphContext>> e : current.getContexts().entrySet()) {
			Graph g = this.graphStorage.getGraphs().get(e.getKey());
			for (GSpanGraphContext context : e.getValue()) {
				int contextId = context.getId();

				// record the tail;
				int oldSavedTailIndex = context.getLastBackwardIndex();
				List<DFSEdge> oldBackwards = context.getBackwards();
				int oldTailBackStart = context.getLastBackwardStart();

				// check if the last vertex has more backward edges to expand
				int savedIndex = context.getLastBackwardIndex();
				List<DFSEdge> backwards = context.getBackwards();
				int tailBackStart = context.getLastBackwardStart();

				if (savedIndex < 0) {// we are here by a new forward edge
					Edge graphEdge = lastEdge.getContextToEdge().get(contextId);
					int graphEdgeFromId = lastEdge.getContextToEdgeFrom().get(contextId);

					int tail = this.getOtherEnd(graphEdge, graphEdgeFromId);
					int tailIndex = context.getIdToIndex().get(tail);

					savedIndex = tailIndex;
					backwards = this.getBackwardEdgeList(g, context, tail, graphEdgeFromId);
					tailBackStart = 0;
				}

				for (int i = tailBackStart; i < backwards.size(); ++i) {
					// we start a search here
					DFSEdge de = backwards.get(i);

					if (newlyAddedEdge.contains(de)) {
						continue; // no repeat
					}
					newlyAddedEdge.add(de);

					// count
					Map<Integer, ArrayList<GSpanGraphContext>> newContexts = countAndGenereateBackward(
							current.getContexts(), de);
					if (newContexts.size() >= min) {
						// we have to update this context and start a new search
						// here
						for (Map.Entry<Integer, ArrayList<GSpanGraphContext>> e2 : newContexts.entrySet()) {
							for (GSpanGraphContext tmpContext : e2.getValue()) {
								tmpContext.setBackwards(backwards);
								tmpContext.setLastBackwardIndex(savedIndex);
								tmpContext.setLastBackwardStart(i + 1);
							}
						}

						DFSTreeNode newNode = new DFSTreeNode(de, current, newContexts);

						subgraphMining(newNode, false, level + 1);

						for (Map.Entry<Integer, ArrayList<GSpanGraphContext>> e2 : newContexts.entrySet()) {
							for (GSpanGraphContext tmpContext : e2.getValue()) {
								tmpContext.setBackwards(oldBackwards);
								tmpContext.setLastBackwardIndex(oldSavedTailIndex);
								tmpContext.setLastBackwardStart(oldTailBackStart);
							}
						}
					}
				}

				// now we create child for all the possible forward edges from
				// the right most path
				List<Integer> rightMostIndex = context.getRightMostPathIndex();
				for (int i = context.getLastPathId(); i >= 0; --i) {
					int index = rightMostIndex.get(i);
					int indexId = context.getIndexToId().get(index);

					List<DFSEdge> forwardEdges = this.getForwardEdgeList(g, context, indexId);
					for (DFSEdge edge : forwardEdges) {
						if (newlyAddedEdge.contains(edge)) {
							continue;
						}
						newlyAddedEdge.add(edge);

						// we clear the context and start new journey
						Map<Integer, ArrayList<GSpanGraphContext>> newContexts = countAndGenereateForward(current.getContexts(), edge);
						if(newContexts.size() >= min){
							//now we set all the new context and head to the next
							for (Map.Entry<Integer, ArrayList<GSpanGraphContext>> e2 : newContexts.entrySet()) {
								for (GSpanGraphContext tmpContext : e2.getValue()) {
									tmpContext.setBackwards(null);
									tmpContext.setLastBackwardIndex(-1);
									tmpContext.setLastBackwardStart(-1);
									tmpContext.extendRightMost(edge.getFromIndex(), edge.getToIndex());
								}
							}
							
							DFSTreeNode newNode = new DFSTreeNode(edge, current, newContexts);
							subgraphMining(newNode, false, level + 1);
							
							//here we do not need to revert
						}
					}
				}
				
				//now we have found all the children for this context, We believe in it has been reverted
			}
		}
	}

	public void run() {
		int total = this.graphStorage.getGraphs().size();
		min = (int) Math.ceil(total * support);
		System.out.println("min = " + min);

		System.out.println(this.graphStorage.getVertexLabelCount());
		System.out.println(this.graphStorage.getEdgeLabelCount());

		// 1. remove infrequent
		for (Map.Entry<Integer, Integer> e : this.graphStorage.getVertexLabelCount().entrySet()) {
			if (e.getValue() < min) {
				graphStorage.removeVertexByLabel(e.getKey());
			} else {
				this.vertexLabel.add(e.getKey());
			}
		}

		for (Map.Entry<Integer, Integer> e : this.graphStorage.getEdgeLabelCount().entrySet()) {
			if (e.getValue() < min) {
				graphStorage.removeEdgeByLabel(e.getKey());
			} else {
				this.edgeLabel.add(e.getKey());
			}
		}

		final Map<Integer, Integer> vertexCount = graphStorage.getVertexLabelCount();
		final Map<Integer, Integer> edgeCount = graphStorage.getEdgeLabelCount();

		// 2. resort label and relabel
		Collections.sort(this.vertexLabel, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				int re = vertexCount.get(o2) - vertexCount.get(o1);
				if (re != 0) {
					return re;
				}

				return o1 - o2;
			}
		});

		Collections.sort(this.edgeLabel, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				int re = edgeCount.get(o2) - edgeCount.get(o1);
				if (re != 0) {
					return re;
				}

				return o1 - o2;
			}
		});

		System.out.println(this.vertexLabel);
		System.out.println(this.edgeLabel);

		Map<Integer, Integer> vertexOldToNew = new HashMap<Integer, Integer>();
		Map<Integer, Integer> edgeOldToNew = new HashMap<Integer, Integer>();
		for (int i = 0; i < this.vertexLabel.size(); ++i) {
			this.newToOldVertexLabel.put(i, this.vertexLabel.get(i));
			vertexOldToNew.put(this.vertexLabel.get(i), i);
		}

		for (int i = 0; i < this.edgeLabel.size(); ++i) {
			this.newToOldEdgeLabel.put(i, this.edgeLabel.get(i));
			edgeOldToNew.put(this.edgeLabel.get(i), i);
		}

		graphStorage.relabelVertex(vertexOldToNew);
		graphStorage.relabelEdge(edgeOldToNew);
		vertexOldToNew = null;
		edgeOldToNew = null;
		System.gc();

		TreeSet<LevelOneDFSEdge> levelOne = new TreeSet<LevelOneDFSEdge>();

		// 3. get S1
		for (Graph g : graphStorage.getGraphs()) {
			for (Edge e : g.getEdges()) {
				if (!e.isRemoved()) {
					// we add it to level one
					int from = e.getFrom();
					int to = e.getTo();
					if (g.getVertexLabel(from) > g.getVertexLabel(to)) {
						int tmp = from;
						from = to;
						to = tmp;
					}

					LevelOneDFSEdge de = new LevelOneDFSEdge(0, 1, g.getVertexLabel(from), e.getLabel(),
							g.getVertexLabel(to));

					NavigableSet<LevelOneDFSEdge> oldOnes = levelOne.subSet(de, true, de, true);
					if (oldOnes.size() >= 2) {
						throw new RuntimeException("Can't has duplication here");
					}

					if (oldOnes.size() == 0) {
						de.addOccurence(g, e, from, to);
						levelOne.add(de);
					} else {
						LevelOneDFSEdge oldOne = oldOnes.first();
						oldOne.addOccurence(g, e, from, to);
					}
				}
			}
		}

		System.out.println(this.newToOldVertexLabel);
		System.out.println(this.newToOldEdgeLabel);

		for (LevelOneDFSEdge de : levelOne) {
			if (de.getContexts().size() < min) {
				// remove this edge directly and continue
				for (Map.Entry<Integer, ArrayList<GSpanGraphContext>> e : de.getContexts().entrySet()) {
					for (GSpanGraphContext context : e.getValue()) {
						context.getFirstEdge().setRemoved(true);
					}
				}
				continue;
			}

			// now we'll start our main algorithm
			DFSTreeNode node = new DFSTreeNode(de, null, de.getContexts());
			node.setLastEdge(de);

			System.out.println(de + ":" + de.getContexts().size());
			// before we start, we fill up other fields of the node's context
			for (Map.Entry<Integer, ArrayList<GSpanGraphContext>> e : de.getContexts().entrySet()) {
				for (GSpanGraphContext context : e.getValue()) {
					//record the right most
					context.initRightMost();
				}
			}

			this.subgraphMining(node, true, 0);
			// now we remove the first edge
			for (Map.Entry<Integer, ArrayList<GSpanGraphContext>> e : de.getContexts().entrySet()) {
				for (GSpanGraphContext context : e.getValue()) {
					context.getFirstEdge().setRemoved(true);
				}
			}
		}

		// here we can output the result
		System.out.println(results.size());
	}

	public List<DFSTreeNode> getResults() {
		return results;
	}

	public void outputResults(PrintStream out) {
		int nextGraph = 0;
		for (DFSTreeNode node : results) {
			GSpanTmpGraph graph = new GSpanTmpGraph(node);
			out.println("t # " + (nextGraph++) + " " + graph.originalNodes.size());
			for (TmpGraphVertex v : graph.vertices) {
				out.println("v " + v.id + " " + this.newToOldVertexLabel.get(v.label));
			}

			for (TmpGraphVertex v : graph.vertices) {
				for (TmpGraphEdge e : v.edges) {
					if (e.from.id == v.id) {
						out.println("e " + e.from.id + " " + e.to.id + " " + this.newToOldEdgeLabel.get(e.label));
					}
				}
			}
		}
		out.println("t # -1");
	}

	public static void main(String[] args) {
		GraphStorage gs = new GraphStorage();
		try {
			gs.readFromFile(args[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}

		GSpan gspan = new GSpan(gs, Float.parseFloat(args[1]));
		gspan.run();
		gspan.outputResults(System.out);
	}
}
