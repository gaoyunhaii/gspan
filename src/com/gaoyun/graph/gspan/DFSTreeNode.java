package com.gaoyun.graph.gspan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DFSTreeNode {
	private DFSEdge lastEdge;
	private DFSTreeNode parent;

	private Map<Integer, ArrayList<GSpanGraphContext>> contexts;

	public DFSTreeNode(DFSEdge lastEdge, DFSTreeNode parent, Map<Integer, ArrayList<GSpanGraphContext>> contexts) {
		super();
		this.lastEdge = lastEdge;
		this.parent = parent;
		this.contexts = contexts;
	}

	public DFSEdge getLastEdge() {
		return lastEdge;
	}

	public void setLastEdge(DFSEdge lastEdge) {
		this.lastEdge = lastEdge;
	}

	public DFSTreeNode getParent() {
		return parent;
	}

	public void setParent(DFSTreeNode parent) {
		this.parent = parent;
	}

	public Map<Integer, ArrayList<GSpanGraphContext>> getContexts() {
		return contexts;
	}

	public void setContexts(Map<Integer, ArrayList<GSpanGraphContext>> contexts) {
		this.contexts = contexts;
	}

	@Override
	public String toString() {
		return "DFSTreeNode [lastEdge=" + lastEdge + "]";
	}
}
