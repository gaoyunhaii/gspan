package com.gaoyun.graph.gspan;

import java.util.HashMap;
import java.util.Map;

import com.gaoyun.graph.storage.Edge;
import com.gaoyun.graph.storage.GraphStorage;

public class DFSEdge implements Comparable<DFSEdge> {
	private int fromIndex;
	private int toIndex;

	private int fromLabel;
	private int eLabel;
	private int toLabel;

	protected Map<Integer, Edge> contextToEdge = new HashMap<Integer, Edge>();
	protected Map<Integer, Integer> contextToEdgeFrom = new HashMap<Integer, Integer>();

	public DFSEdge(int fromIndex, int toIndex, int fromLabel, int eLabel,
			int toLabel) {
		if(toIndex == fromIndex){
			throw new RuntimeException("toIndex == fromIndex");
		}
		
		this.fromIndex = fromIndex;
		this.toIndex = toIndex;
		this.fromLabel = fromLabel;
		this.eLabel = eLabel;
		this.toLabel = toLabel;
	}

	public int getFromIndex() {
		return fromIndex;
	}

	public void setFromIndex(int fromIndex) {
		this.fromIndex = fromIndex;
	}

	public int getToIndex() {
		return toIndex;
	}

	public void setToIndex(int toIndex) {
		this.toIndex = toIndex;
	}

	public int getFromLabel() {
		return fromLabel;
	}

	public void setFromLabel(int fromLabel) {
		this.fromLabel = fromLabel;
	}

	public int geteLabel() {
		return eLabel;
	}

	public void seteLabel(int eLabel) {
		this.eLabel = eLabel;
	}

	public int getToLabel() {
		return toLabel;
	}

	public void setToLabel(int toLabel) {
		this.toLabel = toLabel;
	}

	public Map<Integer, Edge> getContextToEdge() {
		return contextToEdge;
	}

	public void setContextToEdge(Map<Integer, Edge> contextToEdge) {
		this.contextToEdge = contextToEdge;
	}

	public Map<Integer, Integer> getContextToEdgeFrom() {
		return contextToEdgeFrom;
	}

	public void setContextToEdgeFrom(Map<Integer, Integer> contextToEdgeFrom) {
		this.contextToEdgeFrom = contextToEdgeFrom;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + eLabel;
		result = prime * result + fromIndex;
		result = prime * result + fromLabel;
		result = prime * result + toIndex;
		result = prime * result + toLabel;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DFSEdge other = (DFSEdge) obj;
		if (eLabel != other.eLabel)
			return false;
		if (fromIndex != other.fromIndex)
			return false;
		if (fromLabel != other.fromLabel)
			return false;
		if (toIndex != other.toIndex)
			return false;
		if (toLabel != other.toLabel)
			return false;
		return true;
	}

	/**
	 * Assume all the previous edges are the same The comparison is coded
	 * according to page 10
	 */
	@Override
	public int compareTo(DFSEdge o) {
		if (fromIndex == o.fromIndex && toIndex == o.toIndex
				&& fromLabel == o.fromLabel && eLabel == o.eLabel
				&& toLabel == o.toLabel) {
			return 0;
		}

		boolean meIsFront = this.fromIndex < this.toIndex;
		boolean heIsFront = o.fromIndex < o.toIndex;

		if (!meIsFront && heIsFront) {
			return -1;
		}

		else if (!meIsFront && !heIsFront) { // if we are all backward, and
												// since the previous edge are
												// the same, we are from the
												// same vertex
			if (toIndex < o.toIndex
					|| (toIndex == o.toIndex && eLabel < o.eLabel)) {
				return -1;
			}
		}

		else if (meIsFront && heIsFront) { // we are point to some other new
											// vertex, and then we have the same
											// destination
			if (o.fromIndex < fromIndex) { // he is after me because he is
											// gotten by rewarding
				return -1;
			} else if (fromIndex == o.fromIndex) {
				if (fromLabel < o.fromLabel
						|| (fromLabel == o.fromLabel && eLabel < o.eLabel)
						|| (fromLabel == o.fromLabel && eLabel == o.eLabel && toLabel < o.toLabel)) {
					return -1;
				}
			}
		}

		// then we return 1;
		return 1;
	}

	@Override
	public String toString() {
		return "DFSEdge [fromIndex=" + fromIndex + ", toIndex=" + toIndex
				+ ", fromLabel=" + fromLabel + ", eLabel=" + eLabel
				+ ", toLabel=" + toLabel + "]";
	}
}
