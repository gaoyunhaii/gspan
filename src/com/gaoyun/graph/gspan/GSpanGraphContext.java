package com.gaoyun.graph.gspan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gaoyun.graph.storage.Edge;

public class GSpanGraphContext implements Cloneable{
	private static int nextId = 1;

	private int id;
	private Map<Integer, Integer> idToIndex = new HashMap<Integer, Integer>();
	private Map<Integer, Integer> indexToId = new HashMap<Integer, Integer>();
	private Edge firstEdge;

	// the information for the rightmost path
	private ArrayList<Integer> rightMostPathIndex = new ArrayList<Integer>();
	private Map<Integer, Integer> indexToPathId = new HashMap<Integer, Integer>();
	private int lastPathId = 0;

	// record how many of the backward edges of the last pathId has been added
	private int lastBackwardStart = -1;
	private int lastBackwardIndex = -1;
	private List<DFSEdge> backwards = null;

	public GSpanGraphContext() {
		this.id = nextId++;
	}

	public int getId() {
		return id;
	}

	public Map<Integer, Integer> getIdToIndex() {
		return idToIndex;
	}

	public Map<Integer, Integer> getIndexToId() {
		return indexToId;
	}

	public Edge getFirstEdge() {
		return firstEdge;
	}

	public void setFirstEdge(Edge firstEdge) {
		this.firstEdge = firstEdge;
	}

	public void assignIdIndex(int id, int index) {
		this.idToIndex.put(id, index);
		this.indexToId.put(index, id);
	}

	public void removeIdIndex(int index) {
		int id = this.indexToId.get(index);
		this.idToIndex.remove(id);
		this.indexToId.remove(index);
	}

	public List<Integer> getRightMostPathIndex() {
		return rightMostPathIndex;
	}

	public int getLastPathId() {
		return lastPathId;
	}
	
	public void initRightMost(){
		this.rightMostPathIndex.add(0);
		this.rightMostPathIndex.add(1);
		
		this.indexToPathId.put(0, 0);
		this.indexToPathId.put(1, 1);
		this.lastPathId = 1;
	}

	public void extendRightMost(int startIndex, int newIndex) {
		int pathId = this.indexToPathId.get(startIndex);
		if(this.rightMostPathIndex.size() == pathId + 1){
			this.rightMostPathIndex.add(newIndex);
		}
		else if(this.rightMostPathIndex.size() > pathId + 1){
			this.rightMostPathIndex.set(pathId + 1, newIndex);
		}
		else{
			throw new RuntimeException("bad right most path");
		}
		
		this.indexToPathId.put(newIndex, pathId + 1);

		this.lastPathId = pathId + 1;
	}

	public int getLastBackwardStart() {
		return lastBackwardStart;
	}

	public void setLastBackwardStart(int lastBackwardStart) {
		this.lastBackwardStart = lastBackwardStart;
	}

	public int getLastBackwardIndex() {
		return lastBackwardIndex;
	}

	public void setLastBackwardIndex(int lastBackwardIndex) {
		this.lastBackwardIndex = lastBackwardIndex;
	}

	public List<DFSEdge> getBackwards() {
		return backwards;
	}

	public void setBackwards(List<DFSEdge> backwards) {
		this.backwards = backwards;
	}
	
	@Override
	protected GSpanGraphContext clone() throws CloneNotSupportedException {
		GSpanGraphContext context = new GSpanGraphContext();
		
		context.idToIndex = new HashMap<Integer, Integer>(this.idToIndex);
		context.indexToId = new HashMap<Integer, Integer>(this.indexToId);
		context.firstEdge = this.firstEdge;
		
		context.rightMostPathIndex.addAll(this.rightMostPathIndex);
		context.indexToPathId = new HashMap<Integer, Integer>(this.indexToPathId);
		context.lastPathId = this.lastPathId;
		
		return context;
	}
}
