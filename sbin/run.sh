#/usr/bin/env bash
BASE_DIR=`dirname $0`/..;
BASE_DIR=`cd $BASE_DIR;pwd;`;

cp="$BASE_DIR/bin:.";
for j in `ls $BASE_DIR/lib/*.jar`;do
    cp=$j:$cp;
done

java -Xmx12g -Xms12g -classpath $cp com.gaoyun.graph.gspan.GSpan $*

set -x

