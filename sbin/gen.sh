#/usr/bin/env bash

BASE_DIR=`dirname $0`/..;
BASE_DIR=`cd $BASE_DIR;pwd;`;

cp='.';
for j in `ls $BASE_DIR/lib/*.jar`;do
    cp=$j:$cp;
done

set -x
#grep -irl "public static void main(" $BASE_DIR/src | 
find $BASE_DIR/src -name '*.java' |
    while read a;do
        javac -classpath $cp -d $BASE_DIR/bin -sourcepath $BASE_DIR/src $a
    done
set +x
